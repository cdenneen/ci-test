require 'gitlab'
require 'parallel'

Gitlab.endpoint = ENV['GITLAB_API_ENDPOINT'] || 'https://gitlab.com/api/v4'
Gitlab.private_token = ENV['GITLAB_API_PRIVATE_TOKEN'] || raise('GITLAB_API_PRIVATE_TOKEN not set')
httparty = '{verify: false}'
Gitlab.httparty = eval(httparty)

modules = YAML.load_file('managed_modules.yml')

commit=`git rev-parse --short head`.chomp
source_branch = "modulesync-#{commit}"
destination_branch = 'dev'
mr_text = "MR: #{commit} from #{source_branch} to #{destination_branch}"
mr_title = "Merged: #{commit} from #{source_branch} to #{destination_branch}"

Parallel.all?(modules) do |mod|
  merge_requests = Gitlab.merge_requests(mod, {page:1})
  merge_requests.has_next_page?
  merge_requests.next_page
  merge_requests.auto_paginate do |merge_request|
    req=merge_request.to_h
    if req['state'] == 'opened'
      mr_id=req['id']
      sb=req['source_branch']
      db=req['target_branch']
      if source_branch == sb and destination_branch == db
        print "Merging id #{mr_id}: #{mr_text}"
        Gitlab.accept_merge_request(mod,mr_id, { merge_commit_message: "#{mr_title}" })
        exit 0
      end
    end
  end
  merge_requests.auto_paginate

  print "Error: Merge Request NOT Found: #{mod} #{mr_text}\n"
  raise Parallel::Break
  exit 1
end
