require 'gitlab'
require 'git'

Gitlab.endpoint = ENV['GITLAB_API_ENDPOINT'] || 'https://gitlab.com/api/v3'
Gitlab.private_token = ENV['GITLAB_API_PRIVATE_TOKEN']
httparty = '{verify: false}'
Gitlab.httparty = eval(httparty)

@git = Git.open(Dir.pwd)

def write_merge_request_message
  @git.object('HEAD').message
end

modules = YAML.load_file('managed_modules.yml')

commit=`git rev-parse --short head`.chomp
#commit=`cat .git/refs/heads/master | cut -c 1-7`.chomp
puts "*** Commit is #{commit} ***"
log=`git show -s --format=%B #{commit}`

modules.each do |mod|
  branches = Gitlab.branches(mod)
  branches.each do |branch|
    branch_name = branch.name
    if branch_name == "modulesync-#{commit}"
      merge_request = Gitlab.create_merge_request mod, write_merge_request_message, :source_branch => "modulesync-#{commit}", :target_branch => 'dev', :assignee_id => 2, :labels => 'modulesync', :remove_source_branch => true, :description => write_merge_request_message
      puts mod, 'merge_requests', merge_request.iid
    end
  end
end
